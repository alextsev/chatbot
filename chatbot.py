#Creating GUI with tkinter
from tkinter import *
from chat_init import *
import nltk
nltk.download('punkt')
nltk.download('wordnet')
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()
import os
import numpy as np
import webbrowser
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
#SENDGRID_API_KEY = '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'
SENDGRID_API_KEY = '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'

#start#chat_functions
def clc_textbox(sentence):
    sentence_words = nltk.word_tokenize(sentence)
    sentence_words = [lemmatizer.lemmatize(word.lower()) for word in sentence_words]
    return sentence_words

# return bag of words array: 0 or 1 for each word in the bag that exists in the sentence

def bag_of_words(sentence, words, show_details=True):
    # tokenize the pattern
    sentence_words = clc_textbox(sentence)
    # bag of words - matrix of N words, vocabulary matrix
    bag = [0]*len(words)
    #print('bag:',bag,'[0]:',[0],'len:',len(words))# test
    for s in sentence_words:
        for i, w in enumerate(words):
            if w == s:
                # assign 1 if current word is in the vocabulary position
                bag[i] = 1
                if show_details:
                    print("found in bag: %s" % w)
    return(np.array(bag))

def chatbot_predict_class(sentence, model):
    # filter out predictions below a threshold
    p = bag_of_words(sentence, words, show_details=False)
    res = model.predict(np.array([p]))[0]
    prob_threshold = 0.25
    #prob_threshold2 = '0.955'
    for i, r in enumerate(res) :
        if r > prob_threshold :
            #print(r)
            #if(str(round(r, 3))) == prob_threshold2 :
                #results = [[27, r]]
            #else:
            results = [[i, r]]
            print(results)
    #results = [[i, r] for i, r in enumerate(res) if r > prob_threshold]
    # sort by strength of probability
    results.sort(key=lambda x: x[1], reverse=True)

    return_list = []
    for r in results:
        return_list.append({"intent": classes[r[0]], "probability": str(r[1])})
        #print('tag_prob:', return_list)
    return return_list

def end_of_chat():
    message = Mail(
        from_email='hellfireworld688@gmail.com',
        to_emails='hellfire688@hotmail.com',
        subject='ChatBot Notification ' + 'State',
        html_content='Bot: His/Her disorder state is ' + str(state))
    try:
        sg = SendGridAPIClient(api_key=SENDGRID_API_KEY)
        response = sg.send(message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(e.message)

score_maniac = 0
score_depressed = 0
score_mixed_episodes = 0

no_depression = "no depression"
mild_depression = "mild depression"
moderate_depression = "moderate depression"
severe_depression = "severe depression"
very_sever_depression = "very severe depression"

remission = "remission"
minimal_mania = "minimal maniac state"
hypomania = "hypomania"
moderate = "moderate mania"
severe_mania = "severe mania"

mixed_episodes = "mixed episodes"

def Response(predictions, intents_json):
    global score_maniac
    global score_depressed
    global score_mixed_episodes
    global state

    # SENDGRID_API_KEY = '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'
    tag = predictions[0]['intent']
    # print(tag)
    # print('tag:', tag, ' pattern:', predictions[0]['patterns'], ' response:', predictions[0]['responses'])
    intents_list = intents_json['intents']

    # Features ########################
    if(tag == 'blood_pressure'):
        webbrowser.open('https://www.google.com/search?rlz=1C1CHBF_enGR854GR854&sxsrf=ALeKk02aInYZ82Y69sxFdoqsNzVf0uUKbQ%3A1592039227880&ei=O5fkXqCmNYKSsAeZmKjIBA&q=blood+pressure&oq=blood+pressure')
    elif (tag == 'adverse_drug'):
        webbrowser.open('https://www.drugs.com/drug_interactions.html')
    elif (tag == 'hospital_search'):
        webbrowser.open('https://www.google.com/search?q=open+hospitals+near+me&rlz=1C1CHBF_enGR854GR854&oq=open+hospitals+near+me')
    elif (tag == 'pharmacy_search'):
        webbrowser.open('https://www.google.com/search?rlz=1C1CHBF_enGR854GR854&sxsrf=ALeKk01yUeYm3r48jkW_zOifuHnwYMTpbA%3A1592039150966&ei=7pbkXsHLOsyZkwX4wrTQAQ&q=search+for+open+pharmacies+near+me&oq=search+for+open+pharmacies+near+me')
    elif(tag == 'maniac_phase'):
        score_maniac += 1
    elif (tag == 'maniac_aggresive'):
        score_maniac += 4
    elif (tag == 'maniac_hyperactive'):
        score_maniac += 4
    elif (tag == 'maniac_sleep'):
        score_maniac += 5
    elif (tag == 'maniac_body'):
        score_maniac += 3
    elif (tag == 'maniac_logorrhea'):
        score_maniac += 4
    elif (tag == 'maniac_medication'):
        score_maniac += 4
    elif (tag == 'maniac_money'):
        score_maniac += 3
    elif (tag == 'depression_phase'):
        score_depressed += 2
    elif (tag == 'depression_guilt'):
        score_depressed += 2
    elif (tag == 'depression_suicide'):
        score_depressed += 3
    elif (tag == 'depression_sleep'):
        score_depressed += 2
    elif (tag == 'depression_work'):
        score_depressed += 3
    elif (tag == 'depression_anxiety'):
        score_depressed += 2
    elif (tag == 'depression_somatic_anxiety'):
        score_depressed += 3
    elif (tag == 'mixed_episodes_phase'):
        score_mixed_episodes += 2
        print('mixed_episodes object')
    elif (tag == 'story_well'):
        print('end of test')
        end_of_chat()
    elif (tag == 'story_fairly_well'):
        print('end of test')
        end_of_chat()
    elif (tag == 'story_some_degree'):
        print('end of test')
        end_of_chat()
    elif (tag == 'story_not_really_describe'):
        print('end of test')
        end_of_chat()
    elif (tag == 'ideas_out_of_reality'):
        score_maniac += 2
        message = Mail(
            from_email='hellfireworld688@gmail.com',
            to_emails='hellfire688@hotmail.com',
            subject = 'ChatBot Notification '+tag,
            html_content = str(temp))
        try:
            sg = SendGridAPIClient(api_key=SENDGRID_API_KEY)
            response = sg.send(message)
            print(response.status_code)
            print(response.body)
            print(response.headers)
        except Exception as e:
            print(e.message)
    elif (tag == 'Goodbye!'):
        print('end of test')
        end_of_chat()
    elif (tag == 'score_board'):
        ChatLog.config(state=NORMAL)
        ChatLog.config(foreground="#442265", font=("Calibri", 12))
        ChatLog.tag_config('bot', foreground="red")
        ChatLog.insert(END, "Bot: ", "bot", "Your maniac score is " + str(score_maniac) + '\n\n')
        ChatLog.insert(END, "Bot: ", "bot", "Your depression score is " + str(score_depressed) + '\n\n')
        ChatLog.insert(END, "Bot: ", "bot", "Your mixed episodes score is " + str(score_mixed_episodes) + '\n\n')

        # Ham-D score for depression
        if (score_depressed > 0 and score_depressed <= 7 and score_maniac <8 and not(score_mixed_episodes>6)):
            ChatLog.insert(END, "Bot: ", "bot", "You have " + no_depression + '\n\n')
            #print(no_depression)
        elif (score_depressed > 7 and score_depressed <= 13 and score_maniac <8 and not(score_mixed_episodes>6)):
            ChatLog.insert(END, "Bot: ", "bot", "You have " + mild_depression + '\n\n')
            #print(mild_depression)
        elif (score_depressed > 13 and score_depressed <= 18 and score_maniac <8 and not(score_mixed_episodes>6)):
            ChatLog.insert(END, "Bot: ", "bot", "You have " + moderate_depression + '\n\n')
            #print(moderate_depression)
        elif (score_depressed > 18 and score_depressed <= 22 and score_maniac <8 and not(score_mixed_episodes>6)):
            ChatLog.insert(END, "Bot: ", "bot", "You have " + severe_depression + '\n\n')
        elif (score_depressed >= 23 and score_maniac <8 and not(score_mixed_episodes>6)):
            ChatLog.insert(END, "Bot: ", "bot", "You have " + very_sever_depression + '\n\n')
        # YMRS score for mania
        elif ((score_maniac > 1 and score_maniac <= 12) and score_depressed<8 and not(score_mixed_episodes>6)):
            ChatLog.insert(END, "Bot: ", "bot", "You are in " + remission + '\n\n')
            print("You are in remission")
        elif ((score_maniac > 12 and score_maniac <= 19) and score_depressed<8 and not(score_mixed_episodes>6)):
            ChatLog.insert(END, "Bot: ", "bot", "You are in " + minimal_mania + '\n\n')
            print("You are in minimal maniac state")
        elif ((score_maniac > 19 and score_maniac <= 25) and score_depressed<=8 and not(score_mixed_episodes>6)):
            ChatLog.insert(END, "Bot: ", "bot", "You have " + hypomania + '\n\n')
            print("You are in hypomania state")
        elif ((score_maniac > 25 and score_maniac <= 37) and score_depressed<=8 and not(score_mixed_episodes>6)):
            ChatLog.insert(END, "Bot: ", "bot", "You have " + moderate + '\n\n')
            print("You are in moderate maniac state")
        elif (score_maniac >=38 and score_depressed<=8 and not(score_mixed_episodes>6)):
            ChatLog.insert(END, "Bot: ", "bot", "You have " + severe_mania + '\n\n')
            print("You are in severe maniac state")

        # mixed_episodes
        elif ((score_maniac > 7 and score_depressed > 7) or score_mixed_episodes > 6 ):
            ChatLog.insert(END, "Bot: ", "bot", "You have a big chance to have " + mixed_episodes + '\n\n')
        else:
            ChatLog.insert(END, "Bot: ", "bot", "You are ok" + '\n\n')
        #if():
       #     ChatLog.insert(END, "Bot: ", "bot", "You have a big chance to have" + "mixed episodes" + '\n\n')

        ChatLog.config(state=DISABLED)
        ChatLog.yview(END)
    ####################################
    # Save Disorder state
    if (score_depressed > 0 and score_depressed <= 7 and score_maniac < 8 and not (score_mixed_episodes > 6)):
        state = no_depression
        # print(no_depression)
    elif (score_depressed > 7 and score_depressed <= 13 and score_maniac < 8 and not (score_mixed_episodes > 6)):
        state = mild_depression
        # print(mild_depression)
    elif (score_depressed > 13 and score_depressed <= 18 and score_maniac < 8 and not (score_mixed_episodes > 6)):
        state = moderate_depression
    elif (score_depressed > 18 and score_depressed <= 22 and score_maniac < 8 and not (score_mixed_episodes > 6)):
        state = severe_depression
    elif (score_depressed >= 23 and score_maniac < 8 and not (score_mixed_episodes > 6)):
        state = very_sever_depression
    # mania
    elif ((score_maniac > 1 and score_maniac <= 12) and score_depressed < 8 and not (score_mixed_episodes > 6)):
        state = remission
    elif ((score_maniac > 12 and score_maniac <= 19) and score_depressed < 8 and not (score_mixed_episodes > 6)):
        state = minimal_mania
    elif ((score_maniac > 19 and score_maniac <= 25) and score_depressed <= 8 and not (score_mixed_episodes > 6)):
        state = hypomania
    elif ((score_maniac > 25 and score_maniac <= 37) and score_depressed <= 8 and not (score_mixed_episodes > 6)):
        state = moderate
    elif (score_maniac >= 38 and score_depressed <= 8 and not (score_mixed_episodes > 6)):
        state = severe_mania
    # mixed_episodes
    elif ((score_maniac > 7 and score_depressed > 7) or score_mixed_episodes > 6):
        state = mixed_episodes
    else:
        state = "All good!"

    #print(state)

    for i in intents_list:
        if (i['tag'] == tag):
            result = rnd.choice(i['responses'])
            break
        #elif(i['tag'] not in classes):
            #print("not in classes")
            #break

    return result

def chatbot_response(msg):
    predictions = chatbot_predict_class(msg, model)
    res = Response(predictions, intents)
    return res
# end#chat_functions

def send(event=None):
    global temp
    msg = EntryBox.get("1.0", 'end-1c').strip()
    EntryBox.delete("0.0", END)
    temp = msg

    if msg != '':
        ChatLog.config(state=NORMAL)
        ChatLog.tag_config('user', foreground="blue")
        ChatLog.insert(END, "You: ", 'user', msg + '\n\n')
        ChatLog.config(foreground="#442265", font=("Calibri", 12))

        res = chatbot_response(msg)
        ChatLog.tag_config('bot', foreground="red")
        ChatLog.insert(END, "Bot: ", "bot", res + '\n\n')
        #ChatLog.insert(END, "Bot: " + "Your score is " + str(score) + '\n\n')

        ChatLog.config(state=DISABLED)
        ChatLog.yview(END)

def shift_enter(event2=None):
    event2_click = 0

base = Tk()
base.title("Chat Bot Assistant")
base.geometry("400x600")
base.resizable(width=FALSE, height=FALSE)

# Create Chat window
ChatLog = Text(base, bd=0, bg="white", height="8", width="50", font="Calibri")

ChatLog.tag_config('tip', foreground="green")
ChatLog.insert(END, "Tip: ", "tip", "It is essential to write down how you are feeling and what are your plans. Write one sentence at a time!" + '\n\n')
ChatLog.insert(END, "Tip 2: ", "tip", "If you don't know what to write...type question 1...20 to see some options." + '\n\n')

ChatLog.config(state=DISABLED)


# Bind scrollbar to Chat window
scrollbar = Scrollbar(base, command=ChatLog.yview, cursor="dot")
ChatLog['yscrollcommand'] = scrollbar.set


# Create Button to send message
SendBtn = Button(base, font=("Calibri", 12, 'bold'), text="Send", width="12", height=5, bd=0, bg="#6495ed"
                 , activebackground="#64a0ed", fg='#000000', command=send)

base.bind("<Return>", lambda event: send())
base.bind("<Shift-Return>", lambda event2: shift_enter())
base.bind("<Control-Return>", lambda event2: shift_enter())

# Create the box to enter message
EntryBox = Text(base, bd=0, bg="white", width="29", height="5", font="Calibri")

# Place all components on the screen
scrollbar.place(x=376, y=6, height=486)
ChatLog.place(x=6, y=6, height=486, width=370)
EntryBox.place(x=128, y=501, height=90, width=265)
SendBtn.place(x=6, y=501, height=90)

base.mainloop()

