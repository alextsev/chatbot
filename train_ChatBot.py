import nltk
nltk.download('punkt')
nltk.download('wordnet')
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()
import numpy as np
import tensorflow
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM, Dropout, Embedding, GRU, Activation, BatchNormalization
from tensorflow.keras.optimizers import SGD, Adam, Adagrad, Nadam
from chat_init import *
from gensim.models import Word2Vec
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from tensorflow.keras import regularizers
from tensorflow import keras
from tensorflow.keras import layers
from kerastuner.tuners import RandomSearch
from kerastuner.engine.hyperparameters import HyperParameters
import os
import time
from sklearn.utils import shuffle

LOG_DIR = f"{int(time.time())}"

words = []
classes = []
documents = []
ignore_words = ["?", "!", "-", ".", "_", "'"]
data_file = open('intents.json').read()
intents = json.loads(data_file)

for intent in intents['intents']:
    for pattern in intent['patterns']:
        print('patern:', pattern)
        # take each word and tokenize it
        w = nltk.word_tokenize(pattern)
        print('w:', w)
        words.extend(w)
        #Ναρprint('w_ext:', words.extend(w))
        # add documents
        documents.append((w, intent['tag']))

        # add classes to class list
        if intent['tag'] not in classes:
            classes.append(intent['tag'])
            
words = [lemmatizer.lemmatize(w.lower()) for w in words if w not in ignore_words]
words = sorted(list(set(words)))

classes = sorted(list(set(classes)))

print(len(documents), "documents")

print(len(classes), "classes", classes)

print(len(words), "unique lemmatized words", words)

# save with pickle, models and classes
pickle.dump(words, open('words.pkl', 'wb'))
pickle.dump(classes, open('classes.pkl', 'wb'))

# initializing training data
dataset = []
output_empty = [0] * len(classes)
for doc in documents:
    # initializing bag of words
    bag = []
    # list of tokenized words for the pattern
    pattern_words = doc[0]
    # lemmatize each word - create base word, in attempt to represent related words
    pattern_words = [lemmatizer.lemmatize(word.lower()) for word in pattern_words]
    # create our bag of words array with 1, if word match found in current pattern
    for w in words:
        if w in pattern_words:
            bag.append(1)
            # print('loop_w:',w)
            #print('bag_1:',bag)
        else:
            bag.append(0)
            # print('loop_w2:', w)
            #print('bag_2:', bag)


    # output is a '0' for each tag and '1' for current tag (for each pattern)
    output_row = list(output_empty)
    output_row[classes.index(doc[1])] = 1

    dataset.append([bag, output_row])
# shuffle our features and turn into np.array
rnd.shuffle(dataset)
dataset = np.array(dataset)

#print('dataset:',dataset)
#tokenizer = Tokenizer(num_words= 200)
#print('toknzer:',tokenizer)
#tokenizer.fit_on_texts(words)
#print('toknzer2:',tokenizer)
#sequences = tokenizer.texts_to_sequences(words)
#print('seq:',sequences)
#training2 = pad_sequences(sequences, maxlen=50)
#print('pad_seq:', training2)

# create train and test lists. x - patterns, y - intents-tags
x = list(dataset[:, 0])
y = list(dataset[:, 1])

#print('x:', x)
#print(x[0])
#print('y:', y)
#print(y[0])

#word_to_id  = Word2Vec.build_vocab()
vocabulary = len(words)

print("x", x)
print("y", y)
print("Training data created")
x = np.array(x)
y = np.array(y)
#print(train_x.shape)
#x = np.expand_dims(x, axis=2)
#x,y = shuffle(x,y, random_state=123)
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.15, random_state=123)

print('len', len(x))
print('len', len(y))

model = Sequential()
model.add(Dense(250, input_shape=(x[0].shape), activation='relu'))
model.add(Dropout(0.45))
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(len(y[0])))
model.add(Activation('softmax'))


sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)  # loss: 0.0049 - accuracy: 0.98
adam = Adam()  # loss: 0.0229 - accuracy: 0.98

STEPS_PER_EPOCH = 272/5

lr_schedule = tensorflow.keras.optimizers.schedules.InverseTimeDecay(
  0.001,
  decay_steps=STEPS_PER_EPOCH*48,
  decay_rate=1,
  staircase=False)

def get_optimizer():
  return tensorflow.keras.optimizers.Adam(lr_schedule)


model.compile(loss='categorical_crossentropy', optimizer=get_optimizer(), metrics=['accuracy'])
hist = model.fit(x_train, y_train, epochs=250, batch_size=5, verbose=1, validation_data=(x_test,y_test))
# Create model - 3 layers. First layer 128 neurons, second layer 64 neurons and 3rd output layer contains number of neurons
# equal to number of intents to predict output intent with softmax
model.save('chatbot_model.h5', hist)
print("model trained and created")

train_acc = hist.history['accuracy']
train_loss = hist.history['loss']
val_acc = hist.history['val_accuracy']
val_loss = hist.history['val_loss']

#print("number of epochs:", len(train_acc))
epochs = range(len(train_acc))

plt.plot(epochs, train_loss,'r', label='train_loss')
plt.plot(epochs, val_loss,'b', label='val_loss')
plt.title('train_loss vs val_loss')
plt.xlabel("Epochs")
plt.ylabel("Categorical Cross Entropy Loss")
plt.legend()
plt.show()

plt.plot(epochs, train_acc,'r', label='train_acc')
plt.plot(epochs, val_acc,'b', label='val_acc')
plt.title('train_acc vs val_acc')
plt.xlabel("Epochs")
plt.ylabel("Accuracy")
plt.legend()
plt.show()

"""
#randomsearch
def build_model(hp):
    model = Sequential()
    model.add(Dense(hp.Int("input_units",min_value=250,max_value=510,step=10), input_shape=(x_train[0].shape), activation='relu'))
    model.add(Dropout(hp.Float('dropout_1',
                                min_value=0.0,
                                max_value=0.5,
                                default=0.20,
                                step=0.05
                                )))
#for i in range(hp.Int("n_layers", 1, 4)):
    model.add(Dense(hp.Int("input_units2",min_value=200,max_value=300,step=10), activation='relu'))
    model.add(Dropout(hp.Float(
                               'dropout_2',
                                min_value=0.0,
                                max_value=0.5,
                                default=0.20,
                                step=0.05
                                )))
#for j in range(hp.Int("n_layers", 1, 4)):
    model.add(Dense(hp.Int("input_units3",min_value=60,max_value=190,step=10), activation='relu'))
    model.add(Dropout(hp.Float(
                            'dropout_3',
                            min_value=0.0,
                            max_value=0.5,
                            default=0.25,
                            step=0.05
                        )))

    model.add(Dense(len(y[0])))
    model.add(Activation('softmax'))
    model.summary()
    # Compile model. Stochastic gradient descent with Nesterov accelerated gradient gives good results for this model
    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)#loss: 0.0049 - accuracy: 0.98
    adam = Adam()#loss: 0.0229 - accuracy: 0.98
    nadam = Nadam()
    
    STEPS_PER_EPOCH = 272//5

    lr_schedule = tensorflow.keras.optimizers.schedules.InverseTimeDecay(
      0.001,
      decay_steps=STEPS_PER_EPOCH*48,
      decay_rate=1,
      staircase=False)
    
    def get_optimizer():
      return tensorflow.keras.optimizers.Adam(lr_schedule)
      
    model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])
    return model

#fitting and saving the model
#hist = model.fit(x_train, y_train, epochs=250, batch_size=5, verbose=1, validation_data=(x_test, y_test))
#model.save('chatbot_model.h5', hist)




tuner = RandomSearch(
                      build_model,
                      objective='val_accuracy',
                      max_trials = 1,
                      executions_per_trial = 1,
                      directory= LOG_DIR
)

tuner.search(x=x_train,
             y=y_train,
             epochs=250,
             batch_size=5,
             validation_data = (x_test, y_test)
             )


"""

#model.add(Embedding(50000, 128, input_length=172))
#model.add(LSTM(150, return_sequences=True, input_shape=(172,1)))
#model.add(Dropout(0.5))
#model.add(LSTM(128))
#model.add(Dropout(0.3))
#model.add(Dense(64, activation='relu'))
#model.add(Dropout(0.3))
#model.add(Dense(len(train_y[0]), activation='softmax'))

#model.add(GRU(172, activation='relu', return_sequences=True, input_shape=(172,1)))
#model.add(Dropout(0.5))
#model.add(GRU(86, activation='relu'))
#model.add(Dropout(0.3))
#model.add(Dense(64, activation='relu'))
#model.add(Dropout(0.3))
#model.add(Dense(len(train_y[0]), activation='softmax'))



